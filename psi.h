/*----------------------------------------------------------
    -- PSI - Header
    --
    --@praktikum    CEP
    --@semester	    WS13
    --@teamname	    S3T4
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann, Jonas     #2045479
    --@aufgabe	    Aufgabe A6
    --@kontrolleur  Prof. Dr. Schaefers, Michael
-----------------------------------------------------------*/

#include "lpc24xx.h"
#include <stdio.h>
#include "config.h"
#include "ConfigStick.h"
#include "portlcd.h"
#include "fio.h"

#define b(n) (                                               \
    (unsigned char)(                                         \
            ( ( (0x##n##ul) & 0x00000001 )  ?  0x01  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00000010 )  ?  0x02  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00000100 )  ?  0x04  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00001000 )  ?  0x08  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00010000 )  ?  0x10  :  0 )  \
        |   ( ( (0x##n##ul) & 0x00100000 )  ?  0x20  :  0 )  \
        |   ( ( (0x##n##ul) & 0x01000000 )  ?  0x40  :  0 )  \
        |   ( ( (0x##n##ul) & 0x10000000 )  ?  0x80  :  0 )  \
    )                                                        \
)


// prime polynomials for pseudo random number generation
#define   PRIME_POLY_OP1   ( 0x40027 )
#define   PRIME_POLY_OP2   ( 0x1000449 )


// addresses of FPGA registers
#define   adrSR      ( b(000) )
#define   adrOP1     ( b(001) )
#define   adrOP2     ( b(010) )
#define   adrRES     ( b(011) )


int16_t readFPGAviaPSI(void);
void writeFPGAviaPSI(  uint16_t  val );
void init_psi();


