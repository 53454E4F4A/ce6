----------------------------------------------------------
    -- Controller for Square Root Calculation by Trial Substraction
    --@praktikum    CEP
    --@semester     WS13
    --@teamname     S3T4
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann  , Jonas   #2045479
    --@aufgabe      Aufgabe A3 for A6
    --@kontrolleur  Prof. Dr. Schaefers, Michael
-----------------------------------------------------------

library IEEE;
use ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all; 


entity FSM is
    port( 
        clk               : in std_logic;
        reset             : in std_logic;
        start             : in std_logic;
        zero_status       : in std_logic;
        overflow_status   : in std_logic;
        n_overflow_status : in std_logic;
        
        ready             : out std_logic;
        data_ld           : out std_logic;
        reg_en            : out std_logic_vector(1 downto 0);
        erg_en            : out std_logic
    );
end entity FSM;


architecture BEHAVIOR of FSM is

    type   STATES is (IDLE, S0,S1);
    signal STATE      : STATES:= IDLE;
    signal NEXT_STATE : STATES:= IDLE;

begin

    AUE_SN: 
        process(STATE, start, zero_status, overflow_status, n_overflow_status)
        begin
            reg_en  <= "00";
            ready   <=  '0';
            data_ld <=  '0';
            erg_en  <=  '0';

            case STATE is
               
               
                when IDLE => 
                    
                    ready          <= '1'; -- FSM ist bereit	
                    
                    if(START = '1') then 
                        data_ld    <= '1';Fwd: 
                        NEXT_STATE <= S0;
                        
                    else
                        NEXT_STATE <= IDLE;
                        
                    end if ;


                when S0 => 
                     
                    if zero_status = '0' then  
                        if overflow_status = '1' then -- check overflow							
                            reg_en  <= "10";             -- no substract

                        else
                            reg_en  <= "11";              -- substract

                        end if;
                    end if;
                   
                    if n_overflow_status = '1' or zero_status = '1' then   -- N < 0 || r == 0
                        NEXT_STATE <= S1;                                  -- result is ready
                        
                    else
                        NEXT_STATE <= S0;                                  -- one more round
                        
                    end if;	
        
            
                when S1 =>	
                
                    erg_en         <= '1';
                    NEXT_STATE     <= IDLE;

            end case;
        end process AUE_SN;


    STATE_REG: 
        process(clk)
        
        begin
            if clk = '1' and clk'event then
                if reset = '1' then
                    STATE <= IDLE;
                    
                else
                    STATE <= NEXT_STATE;
                   
                end if;
                end if;
        end process STATE_REG;

end BEHAVIOR;


