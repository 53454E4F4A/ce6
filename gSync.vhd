--############################################################################
--###
--###   Demo for CE SS11 related to �C <-> FPGA communication
--###   ================
--############################################################################
--
-- Code belongs to TI4 CE ; Informatik ; HAW Hamburg
-- =================================================
--
--
-- TITLE:          Generic Sync...
-- FILE:           gSync.vhd
-- RELATED CODE:   gcd.vhd
-- PROJECT:        GCD / demo for communication: FPGA/NEXYS2 <-> "SECURITYv2-Board" <-> "TI-Board"/LPC-2468.
-- DESCRIPTION:    ...
--
-------------------------------------------------------------------------------
-- 
-- VHDL:           1076-2002
-- SIMULATOR:      Model Technology ModelSim SE-64 vcom 10.0a Compiler 2011.02 Feb 17 2011
--
-------------------------------------------------------------------------------
--
-- Author(s):
--            name____________      contact___________________________________
--   Shf      Michael Sch�fers      michael.schafers@informatik.haw-hamburg.de
--
-------------------------------------------------------------------------------
--
-- HISTORY
-- =======
--
--   release | date       |      | note
--  ---------+------------+------+---------------------------------------------
--   1.0.0   | 2010.11.15 | Shf  | 1st version for TI4 CE WS10/11
--
-------------------------------------------------------------------------------
--
-- KNOWN PROBLEMS: none
-- NOTEs:          none
-- OPEN POINTS:    Should all be removed in the final version, anyway they have been
--                 marked with: _???_<YYMMDD>  (search/grep for _???_)
--
-------------------------------------------------------------------------------



library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    
    
    
    
    
entity gSync is
    generic (
        nob : natural := 3                          -- Number Of fifo-Bits (resp. FFs in synchronizer FIFO)
    );--}generic
    port (
        synco   : out  std_logic;                   -- Out
        synci   : in   std_logic;                   -- In
        clk     : in   std_logic;                   -- CLocK
        reset   : in   std_logic                    -- Synchronous RESET
    );--}port
    --------------------------------------------------------------------------
    constant mpo : natural := nob-1;                -- Most significant bit POsition
end entity gSync;





architecture rtl of gSync is
    
    signal  syncFIFO_cs  : std_logic_vector( mpo downto 0 )  := (OTHERS=>'0');
    
begin
    
    mixlo:                       -- mixed (combinatorial and sequential) logic
    process ( clk, reset )
    begin
		if  reset='1'  then
			syncFIFO_cs  <=  (OTHERS=>'0');
		elsif clk'event and clk='1' then
			syncFIFO_cs  <=  syncFIFO_cs( mpo-1 downto 0 ) & synci;
		end if;
    end process mixlo;
    --
    -- assign result(s) to outgoing port(s)
    synco <= syncFIFO_cs(mpo);
    
end architecture rtl;


