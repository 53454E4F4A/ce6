----------------------------------------------------------
    -- Datapath computing Square Root by Trial Substraction
    --@praktikum    CEP
    --@semester     WS13
    --@teamname     S3T4
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann  , Jonas   #2045479
    --@aufgabe      Aufgabe A3 for A6
    --@kontrolleur  Prof. Dr. Schaefers, Michael
-----------------------------------------------------------

library IEEE;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity DATAPATH is
    generic (
        width : natural := 12  -- bit width - determines computation accuracy; 13 required to avoid overflows
    );			
    port( 
        clk               : in std_logic;
        reset             : in std_logic;	
        data_in           : in std_logic_vector(width-1 downto 0);
        data_ld	          : in std_logic;
        reg_en            : in std_logic_vector(      1 downto 0);	 
        erg_en            : in std_logic;

        zero_status       : out std_logic;
        overflow_status   : out std_logic;
        n_overflow_status : out std_logic;
        sqrt              : out std_logic_vector(    11 downto 0)
    );
    constant msbpo : natural := width-1;  
end entity DATAPATH;


architecture BEHAVIOR of DATAPATH is

    constant ONE               : std_logic_vector(msbpo downto 0) := "000000000001"; --(0=>'1',others => '0'); --"0000000000000001";
    constant MINUSONE          : std_logic_vector(msbpo downto 0) := "111111111111";

    signal r_cs                : std_logic_vector(   10 downto 0) := (others => '0');
    signal r_ns                : std_logic_vector(   10 downto 0);
    signal s_cs                : std_logic_vector(   10 downto 0) := (others => '0');
    signal s_ns                : std_logic_vector(   10 downto 0);
    signal n_cs                : std_logic_vector(    3 downto 0) := "1010";
    signal n_ns                : std_logic_vector(    3 downto 0);
    signal sign_cs             : std_logic := '0';
    signal sign_ns             : std_logic := '0';
    signal res_cs              : std_logic_vector(   11 downto 0) := (others => '0'); 
    signal res_ns              : std_logic_vector(   11 downto 0);
    signal zero_status_s       : std_logic := '0';
    signal overflow_status_s   : std_logic := '0';
    signal n_overflow_status_s : std_logic := '0';

begin

sqrt <= res_cs;

    -- REGISTER  
    data_reg: 
        process(clk)
        begin
            if clk = '1' and clk'event then
                if reset = '1' then		
                    r_cs    <= (others => '0'); -- Reset R0
                    s_cs    <= (others => '0'); -- Reset R0
                    n_cs    <= (others => '0'); -- Reset R0
                    res_cs  <= (others => '0');
                    sign_cs <= '0';
                    
                else	   
                    r_cs    <= r_ns; 
                    s_cs    <= s_ns; 
                    n_cs    <= n_ns;
                    sign_cs <= sign_ns;
                    res_cs  <= res_ns;
                    
                end if;
            end if;
        end process data_reg;


    -- ALU
    zero_status       <= zero_status_s;
    overflow_status   <= overflow_status_s;
    n_overflow_status <= n_overflow_status_s;

    arith:
        process( r_cs, n_cs, s_cs, sign_cs, res_cs, reg_en, erg_en, data_ld, data_in)

            variable tmp      : std_logic_vector(22 downto 0) := ( others => '0' );
            variable res      : std_logic_vector(11 downto 0) := ( others => '0' );
            variable n_var    : std_logic_vector(3  downto 0) := ( others => '0' );
            variable s_var    : std_logic_vector(10 downto 0) := ( others => '0' );
            variable sub_op1  : std_logic_vector(11 downto 0);
            variable sub_op2  : std_logic_vector(11 downto 0);
            variable sign_var : std_logic := '0';

        begin

            -- INIT
            sub_op1           := (others => '0');
            sub_op2           := (others => '0');
            s_var             := s_cs;
         
            n_var(3 downto 0) := n_cs;		
            sign_var          := sign_cs;
            tmp               := (others => '0');
           
            if data_ld = '1' then
                if data_in(11) = '1' then
                    sub_op1 := not(data_in);
                    sub_op2 := MINUSONE;
             
                else
                    sub_op1 := data_in;
                    sub_op2 := "000000000000";
                    tmp     := (others => '0');
                    
                end if;			
            
            elsif erg_en = '1' then
                if sign_var = '1' then
                    sub_op1(11)          := '1';
                    sub_op1(10 downto 0) := not(s_var);
                    sub_op2              := MINUSONE;
                    tmp                  := (others => '0');
                    
                else
                    sub_op1(11)          := '0';
                    sub_op1(10 downto 0) := s_var;
                    sub_op2              := "000000000000";
                    tmp                  := (others => '0');
                    
                end if;
                
            else
                sub_op1(11)                     := '0';  -- OVERFLOW
                sub_op1(10 downto 0)            := r_cs(10 downto 0);
               
                tmp(11)                         := '0';
                tmp(10 downto 0)                := s_cs;
                tmp                             := std_logic_vector(unsigned(shift_left(unsigned(tmp), 1)));                                 -- S << 1
                tmp(to_integer(unsigned(n_cs))) := '1';                                                                                      -- +2^N
                tmp                             := std_logic_vector(unsigned(shift_left(unsigned(tmp), to_integer(unsigned(n_cs)))));        -- S << N
             -- tmp                             := std_logic_vector(unsigned(shift_right(unsigned(tmp), 11 - to_integer(unsigned(n_cs)) ))); -- S >> N
             
                sub_op2(11)                     := '0';
             -- sub_op2(10 downto 0)            := tmp(to_integer(unsigned(n_cs)) + 11  downto to_integer(unsigned(n_cs)) + 1 );
                sub_op2(10 downto 0)            := tmp(21 downto 11);
                
            end if;
            
            
            -- ARITH		
            res                                := std_logic_vector(unsigned(sub_op1) - unsigned(sub_op2));  -- r - S'
            s_var(to_integer(unsigned(n_var))) := '1';                                                      -- S + 2^N
            n_var                              := std_logic_vector(unsigned(n_var)-1);                      -- N--


            -- FLAGS

            overflow_status_s   <= res(11);
			if n_var = "1111" then
				n_overflow_status_s <= '1';
				
			else
				n_overflow_status_s <= '0';
				
			end if;


            if r_cs = (r_cs'range=>'0') then
                zero_status_s   <= '1';         
                       
            else
                zero_status_s   <= '0';                
                
            end if;


            -- REGISTER
            if n_var = "1111" then 
                n_var := "0000";
            end if;	
            
            if erg_en = '1' then
                r_ns                <= r_cs;
                n_ns                <= n_cs;
                s_ns                <= res(10 downto 0);
                sign_ns             <= sign_cs;
                res_ns(11)          <= sign_cs;
                res_ns(10 downto 0) <= res(10 downto 0);
                
            elsif data_ld = '1' then
                r_ns                <= res(10 downto 0);
                n_ns                <= "1010";
                s_ns                <= (others => '0');
                sign_ns             <= data_in(11);
                res_ns              <= res_cs;
                
            else 
                case reg_en is
                    when "10" =>
                        r_ns        <= r_cs;
                        n_ns        <= n_var(3 downto 0);
                        s_ns        <= s_cs;
                        sign_ns     <= sign_cs;
                        res_ns      <= res_cs;
                        
                    when "11" =>
                        r_ns        <= res(10 downto 0);
                        n_ns        <= n_var(3 downto 0);
                        s_ns        <= s_var;	
                        sign_ns     <= sign_cs;			
                        res_ns      <= res_cs;
                        
                    when others =>
                        r_ns        <= r_cs;
                        n_ns        <= n_cs;
                        s_ns        <= s_cs;
                        sign_ns     <= sign_cs;
                        res_ns      <= res_cs;
                        
                end case;
            end if;
        end process arith;
	
end BEHAVIOR;


