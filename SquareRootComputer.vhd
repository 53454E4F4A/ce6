----------------------------------------------------------
    -- Aggregation of Datapath and Controller
    --@praktikum    CEP
    --@semester     WS13
    --@teamname     S3T4
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann  , Jonas   #2045479
    --@aufgabe      Aufgabe A3 for A6
    --@kontrolleur  Prof. Dr. Schaefers, Michael
-----------------------------------------------------------

library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;
	use ieee.numeric_std.all;


entity SquareRootComputer is   
    generic (
        wwidth    : natural :=  12
    ); --generic    
    port (
        x     : in  std_logic_vector(11 downto 0);
        req   : in  std_logic;
        clk   : in  std_logic;
        sres  : in  std_logic;
        fx    : out std_logic_vector(11 downto 0);
        rdy   : out std_logic
    );
end entity SquareRootComputer;


architecture rtl of SquareRootComputer is

	component datapath is
        generic (
            width : natural := 12  -- bit width - determines computation accuracy; 13 required to avoid overflows
        );			
        port( 
            clk               : in std_logic;
            reset             : in std_logic;	
            data_in           : in std_logic_vector(width-1 downto 0);
            data_ld           : in std_logic;
            reg_en            : in std_logic_vector(      1 downto 0);	 
            erg_en            : in std_logic;

            zero_status       : out std_logic;
            overflow_status   : out std_logic;
            n_overflow_status : out std_logic;
            sqrt              : out std_logic_vector(    11 downto 0)
        );
    end component datapath;
	for all : datapath use entity work.datapath(BEHAVIOR);
    
    
    component FSM is
        port( 
            clk               : in std_logic;
            reset             : in std_logic;
            start             : in std_logic;
            zero_status       : in std_logic;
            overflow_status   : in std_logic;
            n_overflow_status : in std_logic;
            
            ready             : out std_logic;
            data_ld           : out std_logic;
            reg_en            : out std_logic_vector(1 downto 0);
            erg_en            : out std_logic
        );
    end component FSM;
    for all : FSM use entity work.FSM(BEHAVIOR);
    
    signal data_ld           : std_logic;
    signal reg_en            : std_logic_vector(1 downto 0);
    signal erg_en            : std_logic;
    signal zero_status       : std_logic;
    signal overflow_status   : std_logic;
    signal n_overflow_status : std_logic;

begin

    datapath_i : datapath
    generic map(
        width => wwidth     
    )
    port map(
        clk               => clk,
        reset             => sres,
        data_in           => x,
        data_ld	          => data_ld,
        reg_en            => reg_en,
        erg_en            => erg_en,
        zero_status       => zero_status,
        overflow_status   => overflow_status,
        n_overflow_status => n_overflow_status,
        sqrt              => fx
    )
    ; --] datapath
  
  
    FSM_i : FSM
    port map(
        clk               => clk,
        reset             => sres,
        start             => req,
        zero_status       => zero_status,
        overflow_status   => overflow_status,
        n_overflow_status => n_overflow_status,
        ready             => rdy,
        data_ld           => data_ld,
        reg_en            => reg_en,
        erg_en            => erg_en
    )
    ;--]stimuli

end rtl;


