/*----------------------------------------------------------
    -- SSP
    --
    --@praktikum    CEP
    --@semester	    WS13
    --@teamname	    S3T4
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann, Jonas     #2045479
    --@aufgabe	    Aufgabe A6
    --@kontrolleur  Prof. Dr. Schaefers, Michael
-----------------------------------------------------------*/

#include "ssp.h"

void init_spi() {
    // configure GPIO Port0[pin16] for manual signaling of Slave_SELect/Chip_Select
    //  
    PCONP	 |= (1<<21);                                    // enable ssp
    //PCLKSEL1 |= (1<<10);                                  // set clock to cclk (48 MHz)
    //
    // Peripheral CLocK SELection register 1                                                                                    -> UM Chapter 4-3.3.4, Table 57 & 58
    PCLKSEL1 = ( PCLKSEL1 & ~(b(11)<<10)) | (b(01)<<10);    // peripheral clock for SSP0 = 48MHz = Cpu CLocK
    //
    //PINSEL0  |= (2<<30);		                            // SET P0[15] SCK
    //
    //PIN function SELect register 0                                                                                            -> UM Chapter 9-5.5,   Table 130
    PINSEL0 = ( PINSEL0 & ~(b(11)<<30)) | (b(10)<<30);      // Spi ClocK ssp0 (SCK0)
    //
    //GPIO0_IOSET  = (1<<16); 
    //GPIO0_IODIR |= (1<<16);                               // SET GPIO P0[16] to OUTPUT   !!! only here !!!
    //
    PINSEL1 = (PINSEL1 & ~(0x3F)) | (0x28);
    //PINSEL1  |= (2<<2);                                   // SET P0[17] MISO
    //PINSEL1  |= (2<<4);                                   // SET P0[18] MOSI
    // PIN function SELect register 1                                                                                            -> UM Chapter 9-5.5,   Table 131
    // PINSEL1 =    ( PINSEL1 & ~(b(111111)<<0))
    // | (b(00)<<0)                                         // ssp0 Slave SELection (SSEL) is done (manually) via related GPIO register over Port0[pin16]
    // | (b(10)<<2)                                         // MISO_ssp0  (over Port0[pin17])
    // | (b(10)<<4);                                        // MOSI_ssp0  (over Port0[pin18])
    //
    SSP0_CR0 = 0x00C7;                                      // Set Frame size to 8 bit 
    //
    // SSP controller maintains the bus clock high between frames. && SSP controller captures serial data on the second clock transition of the frame, 
    // that is, the transition back to the inter-frame state of the clock line.
    //				                          
    // SSPn Control Register 0                                                                                                   -> UM Chapter 20-6.1, Table 471
    // SSP0_CR0 =   (   0x7<<0)                             // 8 bits/frame ;  set DSS  (Data Size Select)
    // | ( b(00)<<4)                                        // SPI ;           set FRF  (FRame Format)
    // | (  b(1)<<6)                                        //                 set CPOL (Clock out POLarity)
    // | (  b(1)<<7)                                        //                 set CPHA (Clock out PHAse)
    // | (     0<<8);                                       //                 set SCR  (Serial Clock Rate)
    //
    SSP0_CR1 = 0x02;    
    //SSP0_CR1  = (1<<1);			                        // Set SSP to master an enable ssp controler
    //
    // SSPn Control Register 0 -> UM Chapter 20-6.2, Table 472
    // SSP0_CR1 =   ( b(0)<<0 )                             // normal operation ;    set LBM  (Loop Back Mode)
    // | ( b(1)<<1 )                                        // enable SSP0 ;         set SSPE (Synchronous Serial Port controller Enable)
    // | ( b(0)<<2 )                                        // SSP0 ist bus master ; set MS   (Master/Slave mode)
    // | ( b(0)<<3 );                                       // not relevant              SOD  (Slave Output Disable)
    //
    //SSP0_CPSR = 2;				                        // default precaler 2
    SSP0_CPSR = 4;				                            // default precaler 2

}

void read_device_id() {
	
    unsigned int rxDat = 0
    unsigned int id;
    int i;

    start_operation(OPCODE_DEV_ID);
    ssp_send_dummy_bytes(4);

    for( i=0; i<4; i++ ) {
        WAIT_FOR_FIFO_RNE
        rxDat = (rxDat<<8) | SSP0_DR; 
    }

    id = rxDat;   

    printf( "0x1F480000 is expected\n" );                 // -> Atmel AT25DF641 Chapter 12.2, Table 12-1 & 12-2
    printf( "0x%08X is coded AT25DF641 ID\n", id );       // Manufacturer and Device ID of Atmel AT35DF641 flash memory

    i = id & 0xff;
    ssp_send_dummy_bytes(i);

    while ( 0 < i-- ) {                           
        WAIT_FOR_FIFO_RNE
        rxDat = SSP0_DR;                                 

        printf( "%02X ", rxDat );
    }

    printf( "\n" );
    end_operation();

    printf( "0x%02X JEDEC Manufacturer ID\n",                     ( (id>>24)&0xFF ) );
    printf( "0x%02X Family Code\n",                               ( (id>>21)&0x07 ) );
    printf( "0x%02X Density Code\n",                              ( (id>>16)&0x1F ) );
    printf( "0x%02X Sub Code\n",                                  ( (id>>13)&0x07 ) );
    printf( "0x%02X Product Version Code\n",                      ( (id>>8) &0x1F ) );
    printf( "0x%02X Extended Device Information String Length\n", (  id     &0xFF ) );    
}


__inline void ssp_send_dummy_bytes(unsigned int nbytes) {

    int i = 0;

    for(i = 0; i < nbytes; i++ ) {
        WAIT_FOR_FIFO_TNF
        SSP0_DR = 0x00;
    }
}


__inline void ssp_read_dummy_bytes(unsigned int nbytes) {

    unsigned int rxData;
    unsigned int i = 0;

    for(i = 0; i < nbytes; i++) {
        WAIT_FOR_FIFO_RNE
        rxData = SSP0_DR;
    }
}


__inline void start_operation(char opcode) {
	
    unsigned int rxData;

    GPIO0_IOCLR = (1<<16);   	
    WAIT_FOR_FIFO_TNF
    SSP0_DR = opcode;          
    WAIT_FOR_FIFO_RNE
    rxData = SSP0_DR;
}


__inline void send_address(unsigned int address) {
	
    SSP0_DR = ((0xff0000 & address) >> 16); 
    SSP0_DR = ((0x00ff00 & address) >> 8); 	
    SSP0_DR = (0x0000ff & address);
    WAIT_FOR_FIFO_RNE
    ssp_read_dummy_bytes(3);	
}


__inline void end_operation() {
    GPIO0_IOSET = (1<<16);                               
}


__inline void spi_write_enable() {
    start_operation(OPCODE_WRITE_ENABLE);
    end_operation();
}


__inline void spi_global_unprotect() {
    start_operation(OPCODE_WRITE_STATUS_REG1);
    WAIT_FOR_FIFO_TNF
    SSP0_DR = 0;
    ssp_read_dummy_bytes(1);
    end_operation();	
}


void spi_read_data(int addr, int nbytes, unsigned char *data) {
    
    int i = 0;
    
    start_operation(OPCODE_READ);	
    send_address(addr);
    ssp_send_dummy_bytes(2);
    ssp_read_dummy_bytes(2);

    for(i = 0; i < nbytes; i++) {
	    ssp_send_dummy_bytes(1);
	    WAIT_FOR_FIFO_RNE
	    data[i] = SSP0_DR;
    }
    
    end_operation();
}


void spi_start_read(int add){

    int i,dummy;

    FIO0CLR = (1<<16);
    SSP0_DR = 0x0B;
    SSP0_DR = 0xFF & (add>>16);
    SSP0_DR = 0xFF & (add>>8);
    SSP0_DR = 0xFF & (add>>0);
    SSP0_DR = 0x00;

    for(i = 0; i < 5; i++){
        while( !SSP0_SR_RNE  ){};
        dummy=SSP0_DR;
    }
	
    SSP0_DR = 0x00;
    SSP0_DR = 0x00;
}

uint8_t spi_read_once(){

    uint8_t mem;

    SSP0_DR = 0x00;

    while( !SSP0_SR_RNE ) {
        
    }

    mem	= SSP0_DR & 0xFF;
    return mem;	
}


void spi_write_data(int addr, int nbytes, unsigned char *data) {
	
    unsigned int i = 0;

    spi_write_enable();
    spi_global_unprotect();
    spi_write_enable();
    start_operation(OPCODE_WRITE);
    send_address(addr);

    for(i = 0; i < nbytes;  i++){
        WAIT_FOR_FIFO_TNF
        SSP0_DR = data[i];
        ssp_read_dummy_bytes(1);
    }

    end_operation();
}


void spi_erase_block(int addr, int block_size_select) {
	
    char opcode;

    switch(block_size_select){
        case 0:
            opcode = OPCODE_ERASE4;
            break;

        case 1:
            opcode = OPCODE_ERASE32;
            break;

        case 2:
            opcode = OPCODE_ERASE64;
            break;

        default:
            opcode = OPCODE_ERASE4;
    }

    spi_write_enable();
    spi_global_unprotect();
    spi_write_enable();

    start_operation(opcode);
    send_address(addr);
    end_operation();	
}


void spi_erase_consecutive_blocks(int addr, int block_size_select, int nblocks) {
    int b_size, i;

    switch (block_size_select) {
        case 0:
            b_size = B4K_BYTES;
            break;
            
        case 1:
            b_size = B32K_BYTES;
            break;
            
        case 2:
            b_size = B64K_BYTES;
            break;
            
        default:
            b_size = B4K_BYTES;
    }

    for (i = 0; i < nblocks; i++) {
        spi_erase_block(addr, block_size_select);
        spi_wait_ready(); 
        addr += b_size;
    }
} 


void spi_erase_bytes(int addr, int bytes) {

    int n_b64kb, n_b32kb, n_b4kb;
    int eaddr = addr+bytes;

    while(((addr % B32K_BYTES) > B4K_BYTES) && (addr<eaddr )) {
         spi_erase_consecutive_blocks(addr, 0, 1);
         addr += B4K_BYTES;
         bytes -= B4K_BYTES;
    }	

    if((bytes >= B32K_BYTES) && ((addr % B64K_BYTES) < B4K_BYTES)) {
        spi_erase_consecutive_blocks(addr, 1, 1);
        addr += B32K_BYTES;
        bytes -= B32K_BYTES;
    }

    n_b64kb = bytes / B64K_BYTES;
    bytes = bytes % B64K_BYTES;

    n_b32kb = bytes / B32K_BYTES;
    bytes = bytes % B32K_BYTES;
      
    n_b4kb = bytes / B4K_BYTES;
    bytes = bytes % B4K_BYTES;   

    if( bytes > 0) {
        n_b4kb +=1;
    }

    spi_erase_consecutive_blocks(addr, 2, n_b64kb);
    addr += B64K_BYTES * n_b64kb;
    spi_erase_consecutive_blocks(addr, 1, n_b32kb);
    addr += B32K_BYTES * n_b32kb;  
    spi_erase_consecutive_blocks(addr, 0, n_b4kb);    
} 


void spi_write_bytes(int addr, int nbytes, unsigned char *data) {
	
    int next_page_start = 0;
    int newsize = 0;
    int this_page = 0;

    if(addr % 256) {
        this_page = (addr / 256);
        next_page_start = this_page + 1;
        next_page_start *= 256;
        newsize = next_page_start - addr;	

        if(newsize > nbytes) {
            newsize = nbytes;
        }

        printf("writing %d bytes tom 0x%x until next 256b block\n", newsize, next_page_start); 
        spi_write_data(addr, newsize, data);
        spi_wait_ready();	
        data += newsize;
        nbytes -= newsize;	
        addr = next_page_start;
    }

    while(nbytes > 256) {
        printf("writing 256 bytes to 0x%x. %d remaining\n", addr, nbytes);
        spi_write_data(addr, 256, data);
        addr += 256;
        data += 256;
        nbytes -= 256;
        spi_wait_ready();
    } 

    if(nbytes > 0) {
        printf("%d bytes left, writing to 0x%x\n", nbytes, addr);
        spi_write_data(addr, nbytes, data);
        spi_wait_ready();
    }
}


void spi_erase_all() {
    spi_write_enable();
    start_operation(OPCODE_CHIP_ERASE);
    end_operation();
}


void spi_wait_ready() {
	
    int status = 0;

    start_operation(OPCODE_READ_STATUS_REG);

    do {
        ssp_send_dummy_bytes(2);
        WAIT_FOR_FIFO_RNE
        status = SSP0_DR;
        WAIT_FOR_FIFO_RNE
        status = SSP0_DR;
    } while (status & STATUS_READY);

    end_operation();	
}


