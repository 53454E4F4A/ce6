----------------------------------------------------------
    -- Controller 
    --@praktikum    CEP
    --@semester     WS13
    --@teamname     S3T4
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann  , Jonas   #2045479
    --@aufgabe      Aufgabe A6
    --@kontrolleur  Prof. Dr. Schaefers, Michael
-----------------------------------------------------------

library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;
		
	
entity controller is
    generic 
    (
        width : natural := 16
    );--]generic
    port 
    (
        clk         : in    std_logic;
        swreset     : in    std_logic;
        req         : in    std_logic;
        rnw         : in    std_logic;
        iack        : in    std_logic;
        ack         : out   std_logic;
        enCx        : out   std_logic;
        irq         : out   std_logic;
        res         : out   std_logic;
        data        : inout std_logic_vector(11 downto 0)
    );--}port
    constant mpo : natural := width-1;
end entity controller;


architecture rtl of controller is

    signal  ack         : std_logic;
    signal  req_s       : std_logic;
    signal  rnw_s       : std_logic;
    signal  req_ctrl    : std_logic;
    signal  rdy_ctrl    : std_logic;
    signal  oe_s        : std_logic;
    signal  data_in_s   : std_logic_vector(data'range);
    signal  data_out_s  : std_logic_vector(data'range);
    signal  result_ns   : std_logic; 
    signal  result_cs   : std_logic;
    signal  ack_cs      : std_logic;
    signal  ack_ns      : std_logic;
    signal  irq_cs      : std_logic := '0';
    signal  irq_ns      : std_logic;     
    
    
    component gSync is
    generic 
    (
        nob : natural := 2
    );
    port 
    (
        synco   : out  std_logic;
        synci   : in   std_logic;
        clk     : in   std_logic;
        reset   : in   std_logic
    );
    end component gSync;
    for all : gSync use entity work.gSync(rtl);


    component SquareRootComputer is
    port 
    (
        fx      : out std_logic_vector(11 downto 0);
        rdy     : out std_logic;
        x       : in std_logic_vector(11 downto 0);
        req     : in std_logic;
        clk     : in std_logic;
        sres    : in std_logic 
    );
    end component SquareRootComputer;
    for all	: SquareRootComputer use entity work.SquareRootComputer(rtl);

    type STATES is(IDLE, READ1, READ2, READ3, WRITE1, WRITE2, WRITE3, WRITE4);
    signal  state_s         : STATES   := IDLE;
    signal  next_state_s    : STATES   := IDLE;

begin

    syncMReq : gSync
    port map 
    (
        synco  =>  req_s,
        synci  =>  req,
        clk    =>  clk,
        reset  =>  swreset
    );--]syncMReq

    syncMIAck : gSync
    port map 
    (
        synco  =>  ack,
        synci  =>  iack,
        clk    =>  clk,
        reset  =>  swreset
    );--]syncMIAck

    syncMRW : gSync
    port map 
    (
        synco  =>  rnw_s,
        synci  =>  rnw,
        clk    =>  clk,
        reset  =>  swreset
    );--]syncMRW

    squareRoot_i : SquareRootComputer
    port map 
    (
        fx      => data_out_s,
        rdy     => rdy_ctrl,
        x       => data_in_s,
        req     => req_ctrl,
        clk     => clk,
        sres    => swreset
    );


    tristate : process (oe_s, data_out_s)

    begin
        if  oe_s = '1'  then
            data <= data_out_s;
            
        else
            data <= (others=>'Z');
            
        end if;
    end process triState;

    data_in_s <=  data;
    res       <=  irq_cs;


    cntrl : process (state_s, req_s, rdy_ctrl, rnw_s) is
        variable result_v : std_logic;
        variable oe_v     : std_logic;
        variable ack_v    : std_logic;
        variable req_v    : std_logic;

    begin
        result_v := '0';
        oe_v     := '0';
        ack_v    := '0';
        req_v    := '0';

    case state_s is
	    
	    when IDLE =>
		    result_v := '1';

	        if req_s = '1' and rnw_s = '1' then
	            next_state_s <= READ1;
	
	        elsif req_s = '1' and rnw_s = '0' then
	            next_state_s <= WRITE1;
	            
	        else
	            next_state_s <= IDLE;
	            
	        end if;
                
        when READ1 =>
	        oe_v         := '1';
	        next_state_s <= READ2;

        when READ2 =>
	        ack_v := '1';
	        oe_v  := '1';
	
            if req_s = '0' then
                next_state_s <= READ3;
                
            else
                next_state_s <= READ2;
                
            end if;
            
            when READ3 =>
                result_v     := '0';
                oe_v         := '0';
                ack_v        := '0';
                next_state_s <= IDLE;

	        when WRITE1 =>
                ack_v := '1';			

                if req_s = '0' then
                    next_state_s <= WRITE2;
                    
                else
                    next_state_s <= WRITE1;
                    
                end if;
            
            when WRITE2 =>
                ack_v        := '1';
                req_v        := '1';
                next_state_s <= WRITE3;
            
            when WRITE3 =>
                ack_v := '0';
                req_v := '1';
		
                if rdy_ctrl = '1' then
                    next_state_s <= WRITE4;
                    
                else
                    next_state_s <= WRITE3;
                    
                end if;
            
            when WRITE4 =>
                result_v     := '1';
                next_state_s <= IDLE;

            when others => 
                next_state_s <= IDLE;
        end case;

        req_ctrl    <=  req_v;
        oe_s        <=  oe_v;
        result_ns   <=  result_v;
        ack_ns      <=  ack_v;
        
    end process cntrl;


    irq : process (rdy_ctrl, irq_cs, ack) 
        variable  irq_v       : std_logic;
        variable  rdy_p1c_v   : std_logic;
        
    begin
        if rdy_ctrl = '1' then
            irq_v := '1';
            
        elsif ack = '1' then
            irq_v := '0';
            
        else
            irq_v  := irq_cs;
            
        end if;
        
            irq_ns <=irq_v;
            
    end process irq;


    ff: process (clk, swreset) is
    
    begin
        if swreset='1' then
            result_cs   <=  '0';
            ack_cs      <=  '0';
            irq_cs      <=  '0';
            state_s     <=	IDLE;
		
        elsif  clk'event and clk='1'  then
            result_cs   <=  result_ns;
            ack_cs      <=  ack_ns;
            irq_cs      <=  irq_ns;
            state_s     <= 	next_state_s;
		
        end if;
    end process ff;
        
    ack  <=  ack_cs;
    irq  <=  result_cs;
    enCx <=  '1';
    
end architecture rtl;


