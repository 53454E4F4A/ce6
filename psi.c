/*----------------------------------------------------------
    -- PSI
    --
    --@praktikum    CEP
    --@semester	    WS13
    --@teamname	    S3T4
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann, Jonas     #2045479
    --@aufgabe	    Aufgabe A6
    --@kontrolleur  Prof. Dr. Schaefers, Michael
-----------------------------------------------------------*/

#include "psi.h"

static int stateRnW = 0;

void init_psi(){
    // configure "HIGH SPEED GPIO"/"Fast IO" for port 0&1 [Table38]
    // Note: Port 2/3/4 can only be accessed by "Fast IO" [Chap 10.3.1]
    // SCS |=  (b(1)<<0);  // set GPIOM ;  "Fast IO" for port 0&1 [Table38]


    // configure LPC stick pins  -  each configuration requires 2 bits -> hence, 3 means 2 bits equal "11"
    //
    //meaning:
    //direction:    out     out     out     out     out     out
    //Port2-Pin:     13       9       8       5       4       3
    PINSEL4 &= ~((3<<26)|(3<<18)|(3<<16)|(3<<10)|(3<< 8)|(3<< 6));
    //
    //meaning:   dat(15) dat(14) dat(13) dat(12) dat(11)                   adr(2)  adr(1)  adr(0)    REQ     RnW   dat(1)    IRQ     ACK
    //direction:     io      io      io      io      io                      out     out     out     out     out      io      in      in            ___all___   ___out___   ____io___   ____in___
    //Port1-Pin:     20      19      18      17      16                       15      14      13      12      11      10       1       0           "001F-FC03"="0000-F800"|"001F-0400"|"0000-0003"
    PINSEL3 &= ~((3<< 8)|(3<< 6)|(3<< 4)|(3<< 2)|(3<< 0));   PINSEL2 &= ~((3<<30)|(3<<28)|(3<<26)|(3<<24)|(3<<22)|(3<<20)|(3<< 2)|(3<< 0));
    //
    //meaning:    dat(9)  dat(8)  dat(7)  dat(6)  dat(5)  dat(4)   IACK                  SWreset   dat(3)  dat(2)  dat(0) dat(10)
    //direction:     io      io      io      io      io      io     out                      out      io      io      io      io                    ___all___   ___out___   ____io___   ____in___
    //Port0-Pin:     30      29      22      21      20      19      18                       15      14      13       5       4                   "607C-E030"="0004-8000"|"7078-6030"|"00000000"
    PINSEL1 &= ~((3<<28)|(3<<26)|(3<<12)|(3<<10)|(3<< 8)|(3<< 6)|(3<< 4));   PINSEL0 &= ~((3<<30)|(3<<28)|(3<<26)|(3<<10)|(3<< 8));
    // configure data direction (and enable drivers of static outputs)
    FIO2DIR |=     0x00002338;
    //FIO2DIR &=  ~( 0x00000000 | 0x00000000 );  // IOs as INPUTs in the beginning
    FIO1DIR |=     0x0000F800;
    FIO1DIR &=  ~( 0x00000003 | 0x001F0400 );  // IOs as INPUTs in the beginning
    FIO0DIR |=     0x00048000;
    FIO0DIR &=  ~( 0x00000000 | 0x60786030 );  // IOs as INPUTs in the beginning
    // enable communication on (security) board level and configure communication directions
    FIO2CLR  =                  // static directions:
                  (b(1)<< 8)    //   IOB4: T/nR  resp.  FPGA <- �C  for  REQ, RnW, ADR, SW_reset  - always
                | (b(1)<< 9)    //   IOB5: T/nR  resp.  FPGA <- �C                                - always
    ;
    FIO2SET  =                  // dynamic directions:
                  (b(1)<< 3)    //   IOB1: T/nR  resp.  FPGA -> �C                                - at beginning
                | (b(1)<< 4)    //   IOB2: T/nR  resp.  FPGA -> �C                                - at beginning
                                // static directions:
                | (b(1)<< 5)    //   IOB3: T/nR  resp.  FPGA -> �C  for  IRQ, ACK                 - always
                                // enable:
                | (b(1)<<13)    //   �C<->FPGA communication                                      - always
    ;
    // now, FPGA is accessible
    // hence, first of all
    // just for safety's sake:  reset FPGA to ensure that it is in well defined state
    //-----------------------------vvvvvvvvvvvvvvvvvvvvv
    FIO1CLR  =  (b(11)<<11);    // clear "REQ"   and signal "WRITE" without doing it - this way force others to switch drivers off

    //-----------------------------^^^^^^^^^^^^^^^^^^^^^
    FIO1CLR  =  ( b(1)<<15);    // deactivate "SW reset"
    //
    // force READ setting by reading FPGA (SR)
    stateRnW  =  0;                  // "last state" "was" write.  this(!) in combination with succeeding read  forces read settings
}


void writeFPGAviaPSI(  uint16_t  val ) {    // value to be written into FPGA
    //
    uint32_t  txdata0;                      // TxData - temporary working variable, any kind of data to be transmitted into FPGA
    uint32_t  txdata1;			 
    //
    if ( stateRnW == 0 ){
        FIO1CLR  =  (b(1)<<11);     // signal FPGA that it is written
        //
        // control IOBs on security board
        FIO2CLR  =    (b(1)<< 4)   // IOB2: T/nR  resp.  clear "FPGA -> �C"
                    | (b(1)<< 3)   // IOB1: T/nR  resp.  clear "FPGA -> �C"
        ;
        //
        // control �C -> enable output drivers
        FIO1DIR |=  ( (1 << 16) | (1 << 10) );    // ADR is enabled - otherwise: 0x001FE400    0x001F0400=  1111 1000 0010 0000 0000 0   0x001FE400=1111 1111 0010 0000 0000 0
        FIO0DIR |=  0x60786030;	   //30,29,22,21,20,19,14,13,5,4 = '1'
         // now WRITE (�C->FPGA) is configured
        stateRnW  =  -1;           // mark new state
    }
    // prepare signals: control & data
    txdata0  =  // DATA-Value                            TxDAT <<<--------------- DATA
        (  ( val & (b(11)   << 10) ) << 19  ) |		// Bit 11:10 to 30:29
        (  ( val & (b(1111) <<  6) ) << 13  ) |		// Bit  9: 6 to 22:19
        (  ( val & (b(11)   <<  3) ) << 10  ) |		// Bit  4: 3 to 14:13
        (  ( val & (b(11)   <<  0) ) <<  4  );		// Bit  1: 0 to  5: 4
    ;
    //
    FIO0CLR  =  0x60786030;
    FIO0SET  =  txdata0;
    //
    txdata1 =   // DATA-Value                            TxDAT <<<--------------- DATA
        (  ( val & (b(1)    <<  5) ) << 11  ) |		// Bit  5: 5 to 16:16
        (  ( val & (b(1)    <<  2) ) <<  8  );		// Bit  2: 2 to 10:10
    ;
    //
    FIO1CLR  =  ( (1 << 16) | (1 << 10) );
    FIO1SET  =  txdata1;
    // handle transaction by 4 phase handshake
    //
    FIO1SET  =  (b(1)<<12);                          // phase1 :  send out REQuest, marking all signals valid               (handover data to FPGA)
    while ( (FIO1PIN & (b(1)<<0))  == 0 );           // phase2 :  wait for ACKnowledge high                                 (FPGA has taken data)
    FIO1CLR  =  (b(1)<<12);                          // phase3 :  handshake for ACK with REQuest low, marking data is gone  (bus is free again)
    while ( (FIO1PIN & (b(1)<<0))  != 0 );           // phase4 :  wait for ACKnowledge low                                  (FPGA is ready for next access)
}


int16_t readFPGAviaPSI(){
    //
    uint32_t  rxData1;  // RxDATA - temporary working variable, any kind of data to be received from FPGA
    uint32_t  rxData2;  // RxDATA - temporary working variable, any kind of data to be received from FPGA
    // check configuration   resp.  handle configuration if required
    //
    if ( stateRnW != 0 ){
        // still write acces configured, hence:
        //
        // configure READ access
        // =====================
        // switch direction   starting from  old_driving_source/new_traget  to  old_target/new_driving_source
        //
        // control �C
        FIO1DIR &=  ~0x001F0400;
        FIO0DIR &=  ~0x60786030;
        //
        // control IOBs on security board
        FIO2SET  =    (b(1)<< 4)   // IOB2: T/nR  resp.  set "FPGA -> �C"
                    | (b(1)<< 3)   // IOB1: T/nR  resp.  set "FPGA -> �C"
        ;
        //
        // control FPGA
        FIO1SET  =  (b(1)<<11);    // signal FPGA that it is read
        // now READ (�C<-FPGA) is configured
        stateRnW  =  0;            // mark new state
    }
    //
    // handle transaction by 4 phase handshake
    //
    FIO1SET  =  b(1)<<12;                       // phase1 :  send out REQuest, marking all signals valid                  (hand over bus to FPGA)
    // while ( (FIO1PIN & (b(1)<<0))  == 0 );   // phase2 :  wait for ACKnowledge high   resp.   result                   (FPGA is driving bus)
    //
    rxData1 = FIO0PIN;                          // get data part 1  - read FIO0PIN
    rxData2 = FIO1PIN;                          // get data part 2  - read FIO1PIN
    //
    FIO1CLR  =  b(1)<<12;                       // phase3 :  handshake for ACK with REQuest low, marking results taken and request bus back
    // while ( (FIO1PIN & (b(1)<<0))  != 0 );   // phase4 :  wait for ACKnowledge low    resp.                            (bus free again)  
    //
    //siehe Pin belegung CETI4_no13_vw12_cw23_120607_v00.pdf S.36
    //
    // evaluate data and return it
    int16_t data = 
        (  ( rxData1 & (b(1)   << 29) ) >> 19  ) |		// Bit 30:29 to 11:10
        (  ( rxData1 & (b(1111) << 19) ) >> 13  ) |		// Bit 22:19 to  9: 6
        (  ( rxData2 & (b(1)    << 16) ) >> 11  ) |		// Bit 16:16 to  5: 5
        (  ( rxData1 & (b(11)   << 13) ) >> 10  ) |		// Bit 14:13 to  4: 3
        (  ( rxData2 & (b(1)    << 10) ) >>  8  ) |		// Bit 10:10 to  2: 2
        (  ( rxData1 & (b(11)   <<  4) ) >>  4  );		// Bit  5: 4 to  1: 0
    //
    if(rxData1 & (1 << 30)) {
        data |= 0xF800;  
    } else {
	    data &= 0xFFF;
    }
    //
    return data;
}


