/*----------------------------------------------------------
    -- MusicPlayer
    --
    --@praktikum    CEP
    --@semester	    WS13
    --@teamname	    S3T4
    --@name         Schaeufler, Jonas   #2092427
    --              Bergmann, Jonas     #2045479
    --@aufgabe	    Aufgabe A6
    --@kontrolleur  Prof. Dr. Schaefers, Michael
-----------------------------------------------------------*/

#include <stdio.h>
#include "lpc24xx.h"
#include "config.h"
#include "ConfigStick.h"
#include "portlcd.h"
#include "fio.h"
#include "psi.h"
#include "ssp.h"

#define FIFO_SIZE 16
#define SPI_SIZE ((0x800000)-1)
#define FIFO_INDX_MSK   ( FIFO_SIZE - 1 )
#define  S0   ( (FIO1PIN & (1<< 8))  == 0 )
#define  S1   ( (FIO1PIN & (1<< 9))  == 0 )
#define  S2   ( (FIO1PIN & (1<<10))  == 0 )
#define  S3   ( (FIO1PIN & (1<<11))  == 0 )
#define  S4   ( (FIO1PIN & (1<<12))  == 0 )
#define  S5   ( (FIO1PIN & (1<<13))  == 0 )
#define  S6   ( (FIO1PIN & (1<<14))  == 0 )
#define  S7   ( (FIO1PIN & (1<<15))  == 0 )


#define b(n) (                                               \
(unsigned char)(                                         \
        ( ( (0x##n##ul) & 0x00000001 )  ?  0x01  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00000010 )  ?  0x02  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00000100 )  ?  0x04  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00001000 )  ?  0x08  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00010000 )  ?  0x10  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00100000 )  ?  0x20  :  0 )  \
    |   ( ( (0x##n##ul) & 0x01000000 )  ?  0x40  :  0 )  \
    |   ( ( (0x##n##ul) & 0x10000000 )  ?  0x80  :  0 )  \
    )                                                        \
)


void __attribute__ ((interrupt("IRQ"))) isr_pwm(void);


volatile static int fifo_left[FIFO_SIZE] = {0};
volatile static int fifo_right[FIFO_SIZE] = {0};
volatile int fifo_rdIdx = 0;
volatile int fifo_wrIdx = 0;
volatile int fifo_underflow = 0;
volatile int fifo_nextWrite = 1;


const int period = 1632;  //72000000/44100


__inline void init(){

    CCLKCFG = 3; //72MHz=3
    init_psi();
    init_spi();

	
    SCS |=  (b(1)<<0);              // set GPIOM ;  "Fast IO" for port 0&1 [Table38]	
    FIO0DIR |= (b(1)<<16);          // GPIO Port0[pin16] has to be output
    FIO0SET  = (b(1)<<16);          // deselect slave/flash ;  connected flash_CS is low active			 
    //SCS |=  (b(1)<<0);            // set GPIOM ;  "Fast IO" for port 0&1 [Table38]
    
    
    // configure Port1 Pin4/3/2/1/0 as output and clear them
    //FIO1DIR = (b(11111)<<0);        // set   Bits 4,..,0 of Port 1 as output                    -> UM Chapter 10-6, Table 158 & lpc24xx.h
    //FIO1CLR = (b(11111)<<0);        // clear Bits 3,..,0 of Port 1                              -> UM Chapter 10-6, Table 158 & lpc24xx.h
    
    
    
    
    
    // Vectored Interrupt Controller (VIC) -> see UM Chapter 7: LPC24XX Vectored Interrupt Controller (VIC)
    // ===================================
    //
    // configure VIC that IRQs from PWM0&1 are handled in appropriate way
    // PWM0&1 IRQ line is connected to VIC channel 8                                            -> UM Chapter 7-4,    Table 116 (& 117)
    // Hence, VICVect*8 is related to PWM0&1
    //
    VICIntSelect      &=  ~(b(1)<<8);          // PWM0&1 assigned to IRQ category and NOT FIQ   -> UM Chapter 7-3.6,  Table 108 (& 117)
    VICVectPriority8  =  5;                    // assign Prio 5 for PWM0&1                      -> UM Chapter 7-3.10, Table 112 (& 102, 117)
    VICVectAddr8      =  (int)( isr_pwm );     // bind "pwm isr" to PWM0&1 IRQ                  -> UM Chapter 7-3.11, Table 113 (& 102, 117)
    VICIntEnable      |=  (b(1)<<8);           // enable interrupt for PWM0&1 IRQ               -> UM Chapter 7-3.4,  Table 106 (& 117)
    
    
    
    
    
    // Pulse Width Modulator (PWM) -> see UM Chapter 25: LPC24XX Pulse Width Modulator PWM0/PWM1
    // ===========================
    //
    // configure PWM1

    // Power CONtrol for Peripherals register                                                   -> UM Chapter 4-3.4.9, Table 63
    PCONP   |= (b(1)<<6);                                   // turn PWM1 on

    PCLKSEL0 = ( PCLKSEL0 & ~(b(11)<<12)) | (b(01)<<12);    //cpu clock

    PINSEL4  =   ( PINSEL4 & ~(b(1111)<<0) )                // reset settings for Port-2[pin0/1] and set ...
               | (b(01)<<0)                                 // ... Port-2[pin0] as PWM1.1
               | (b(01)<<2);                                // ... Port-2[pin1] as PWM1.2
		       
    PWM1_MCR =   (b(011)<<0) ;                              // do NOT stop timer on MR0 match [#2],  reset timer on MR0 match [#1],  !!! NO IRQ on MR0 match [#0] !!!
               //| (b(001)<<9);                             // IRQ on MR3 match [#9] ; single purpose of MR3 is to produce IRQ as workaround for "MR0-IRQ"-problem
               
               
    // PWM1 PwmControlRegister                                                                   -> UM Chapter 25-6.6,  Table 563
    PWM1_PCR =   (b(1)<<9)                                  // PWM1.1 enabled
               | (b(0)<<2)                                  // PWM1.2 single edge control
               | (b(1)<<10);                                // PWM1.2 enabled
               
    //
    // PWM1 Pwm Control Register                                                                -> UM Chapter 25-6.6,  Table 563
    // PWM1_PCR =   (b(1)<<9)                               // PWM1.1 enabled [#9]  ;   (always single edge control)
               // | (b(1)<<10) | (b(0)<<2);                 // PWM1.2 enabled [#10] ,    PWM1.2 single edge control [#2]

    PWM1_PR=0;												                                  //-> UM Chapter 25-6,    Table 557, cpu nicht andern

    PWM1_MR0 = period;   //72000000/44100                   // set cycle period / MR0 is reponsible for period  1632 schritte

    PWM1_LER = (b(111)<<0);  


    PWM1_MR1 = 0;                                           // set "duty cylce"/"falling edge"  for PWM1.1/MR1 - just an example
    PWM1_MR2 = 0;                                           // set "duty cylce"/"falling edge"  for PWM1.2/MR2 - just an example
    //maybe dont need to use PWM1_MR3 = 2;                  // "IRQ trigger":  HERE (0+1 =) 1, ..., 7 (= max-1) works fine ;  MR3 is used as workaround for "MR0-IRQ"-problem

    PWM1_IR  =  0x07ff;                                    // reset all pendings IRQs
    //


    PCLKSEL0 = ( PCLKSEL0 & ~(3<<22)) | (1<<22);            // Peripheral CLocK for DAC = 48MHz = Cpu CLocK                     -> UM Chapter 4-3.3.4, Table 56 & 58
    PINSEL1  = ( PINSEL1  & ~(3<<20)) | (2<<20);            // PIN funtion SELected for "Port-0[pin26]" as AOUT                 -> UM Chapter 29-1 -> 9-5.2, Table 131 (& 598)


    // PWM1 Timer Control Register (part#2)                                                                                     -> UM Chapter 25-6.2,  Table 559
    //PWM1_TCR = (b(01001)<<0);                             // PWM (mode) Enable(d) [#3],  Counter Enable [#0]
	
}


void __attribute__ ((interrupt("IRQ"))) isr_pwm(void) {
	
	FIO1SET=(1<<13);
	PWM1_IR = (b(1)<<0);                          // Interrupt flag for PWM match channel 0.

	if (fifo_rdIdx != fifo_wrIdx) {
        PWM1_MR1 = fifo_right[fifo_rdIdx];
        PWM1_MR2 = fifo_left[fifo_rdIdx]; 
        PWM1_LER = (b(1)<<2) | (b(1)<<1);
        //DACR = fifo_left[fifo_rdIdx] & 0xFFC0;
        fifo_rdIdx = (fifo_rdIdx + 1) & FIFO_INDX_MSK;
		
	} else {	
        //FIO1SET=(1<<14);
        fifo_underflow = -1;

	}
 	
    FIO1CLR=(1<<13);		
    VICVectAddr = 0;                              // mark end of ISR  resp. trigger update of VIC priority logic                -> UM Chapter 3-3.11, Table 113
    
}


int main(void){
	
    int i = 0;
    int resl;
    int resr;


    BaseStickConfig();

    #ifdef LCD_SUPPORT     
        LCD_puts( "CEA6" );
    #endif

    init();


    FIO1DIR |= (1<<14);	
    FIO1DIR |= (1<<15);	
    FIO1CLR = (1<<14);
    FIO1CLR = (1<<15);	

    spi_start_read(0x000000); 

    uint16_t txdat = 0;
    uint16_t rxdat = 0;
    int pos = 0;


    rxdat=spi_read_once();					
    rxdat |= spi_read_once() <<8;			
    pos += 2;		
	
	
    for(i=0;i<10;i++) {

        txdat = rxdat >> 4;                 
        writeFPGAviaPSI(txdat);

        rxdat = spi_read_once();					
        rxdat |= spi_read_once() << 8;		

        resl = (uint16_t) ((readFPGAviaPSI() + 2048) << 4);

        txdat = rxdat >> 4;                 
        writeFPGAviaPSI(txdat);

        rxdat = spi_read_once();							
        rxdat |= spi_read_once() << 8;			
        pos += 4;

        resr = (uint16_t) ((readFPGAviaPSI() + 2048) << 4);
	
        fifo_left[fifo_wrIdx] = (period * resl) >> 16;
        fifo_right[fifo_wrIdx] = (period * resr) >> 16;
        fifo_wrIdx = (fifo_wrIdx + 1) & FIFO_INDX_MSK;
        fifo_nextWrite = (fifo_nextWrite + 1) & FIFO_INDX_MSK;
    }
	
	
	while(!S0) {
	
	}
	
	PWM1_TCR = (b(01001) << 0); 
	
	
    while(1) {	
                              
        rxdat = spi_read_once();							
        rxdat |= spi_read_once() << 8;	
        pos += 2;				

        while(pos <= SPI_SIZE) {

            txdat = rxdat >> 4;                 
            writeFPGAviaPSI(txdat);

            rxdat = spi_read_once();					
            rxdat |= spi_read_once() << 8;		

            resl = (uint16_t) ((readFPGAviaPSI() + 2048) << 4);

            txdat = rxdat >> 4;                 
            writeFPGAviaPSI(txdat);

            rxdat = spi_read_once();							
            rxdat |= spi_read_once() << 8;			
            pos += 4;

            resr = (uint16_t) ((readFPGAviaPSI() + 2048) << 4);


            while(fifo_nextWrite == fifo_rdIdx) {

                if(S1) {
                    FIO1CLR=(1<<15);

                }

                FIO1SET=(1<<15);
            }


            fifo_left[fifo_wrIdx] = (period * resl) >> 16;
            fifo_right[fifo_wrIdx] = (period * resr) >> 16;
            fifo_wrIdx = (fifo_wrIdx + 1) & FIFO_INDX_MSK;
            fifo_nextWrite = (fifo_nextWrite + 1) & FIFO_INDX_MSK;

            if(S1) {
                FIO1CLR=(1<<15);
            }
        }
    }
}


